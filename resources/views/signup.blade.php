<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Signup Page</title>
</head>
<body>
    <h2>Signup Here</h2>
    <div>
       <form action="{{url('/saveuser')}}" method="post">
            @csrf
            <input type="text" name="username" placeholder="Enter your name" value="{{old('username')}}">
            @if($errors->has('username'))
                {{$errors->first('username')}}
            @endif
            <br/><br/>
            <input type="email" name="useremail" placeholder="Enter your email" value="{{old('useremail')}}">
            @if($errors->has('useremail'))
                {{$errors->first('useremail')}}
            @endif
            <br/><br/>
            <input type="number" name="usermobile" placeholder="Enter your mobile" value="{{old('usermobile')}}">
            @if($errors->has('usermobile'))
                {{$errors->first('usermobile')}}
            @endif
            <br/><br/>
            <input type="password" name="userpass" placeholder="Enter your password" value="{{old('userpass')}}">
            @if($errors->has('userpass'))
                {{$errors->first('userpass')}}
            @endif
            <br/><br/>
            <input type="submit" name="submit" value="Signup Now">
       </form>
    </div>

    </form>
</body>
</html>