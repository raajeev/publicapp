<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
class UserController extends Controller
{
    public function index()
    {
        return view('signup');
    }
    public function create(Request $req){
        $validateData = $req->validate([
            'username'=>'required|regex:/[a-zA-Z]/',
            'useremail'=>'required|email|unique:users,email',
            'usermobile'=>'required|min:10|max:10',
            'userpass'=>'required',
        ],[
            'username.required'=>'Please enter your name',
            'username.regex'=>'Only characters are allowed',
            'useremail.required'=>'Please enter your email',
            'useremail.email'=>'Please enter a valid email',
            'useremail.unique'=>'Email Id is already exits',
            'usermobile.required'=>'Please enter your mobile no',
            'userpass.required'=>'Please enter your password',
        ]);

        $useName = $req->username;
        $userEmail = $req->useremail;
        $userMobile = $req->usermobile;
        $userPass = $req->userpass;
        $insertData = User::create([
            'name'=>$useName,
            'email'=>$userEmail,
            'mobile'=>$userMobile,
            'password'=>Hash::Make($userPass),

        ]);
        if($insertData)
        {
            echo 'Signup successfully login now';
        }else{
            echo 'Something went wrong! Please try again later';
        }
        

    }

}
